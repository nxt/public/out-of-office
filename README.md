# out-of-office for other user

This script sets an out of office for another user in your Google Workspace domain.

## Installation

```bash
git clone git@gitlab.com:nxt/public/out-of-office.git
python3 -m venv .venv
.venv/bin/python3 -m pip install -U pip setuptools wheel
.venv/bin/python3 -m pip install -r requirements.txt
```

## Preparation

You need to get a `service-account.json` file, as described here:
https://developers.google.com/identity/protocols/oauth2/service-account#python

- Also configure domain-wide delegation.
- The API we require is `'https://www.googleapis.com/auth/gmail.settings.basic'`.
- You need a Google Cloud project.
- You also need to enable the Gmail API for your Google Cloud project.

## Configuration

Also edit the `ACCOUNT` and `TEXT` and `SUBJECT`.
Change the duration in days with `DURATION_D`.

## Run

Then run with `.venv/bin/python3 main.py`

# License

This code is [licensed under MIT license](LICENSE).
