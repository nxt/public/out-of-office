from __future__ import print_function

from datetime import datetime, timedelta

from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from numpy import long

SCOPES = ['https://www.googleapis.com/auth/gmail.settings.basic']
SERVICE_ACCOUNT_FILE = 'service-account.json'

ACCOUNT = 'some.one@your.domain'
DURATION_D = 365
SUBJECT = "Abwesenheitsnotiz | Out Of Office notice |"
TEXT = "<p>Aufgrund eines Unfalles bin ich bis auf Weiteres abwesend. " \
       "Bitte leiten Sie Ihr Anliegen an nothing@nxt.engineering weiter." \
       "<br/>" \
       "<br/>" \
       "Vielen Dank für Ihr Verständnis." \
       "</p>" \
       "<hr>" \
       "<p>Because of an accident I'm out of office. " \
       "Please forward your email to nothing@nxt.engineering." \
       "<br/>" \
       "<br/>" \
       "Thank you for your understanding." \
       "</p>"


def enable_auto_reply():
    creds_service = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE, scopes=SCOPES)

    creds = creds_service.with_subject(ACCOUNT)

    """Enable auto reply.
    Returns:Draft object, including reply message and response meta data.

    Load pre-authorized user credentials from the environment.
    TODO(developer) - See https://developers.google.com/identity
    for guides on implementing OAuth2 for the application.
    """

    try:
        # create gmail api client
        service = build('gmail', 'v1', credentials=creds)

        epoch = datetime.utcfromtimestamp(0)
        now = datetime.now()
        start_time = (now - timedelta(days=1) - epoch).total_seconds() * 1000
        end_time = (now + timedelta(days=DURATION_D) - epoch).total_seconds() * 1000
        vacation_settings = {
            'enableAutoReply': True,
            'responseSubject': SUBJECT,
            'responseBodyHtml': TEXT,
            'restrictToContacts': False,
            'restrictToDomain': False,
            'startTime': long(start_time),
            'endTime': long(end_time)
        }

        # pylint: disable=E1101
        response = service.users().settings().updateVacation(
            userId=ACCOUNT, body=vacation_settings).execute()
        print(F'Enabled AutoReply with message: '
              F'{response.get("responseBodyHtml")}')

    except HttpError as error:
        print(F'An error occurred: {error}')
        response = None

    return response


if __name__ == '__main__':
    enable_auto_reply()
